import logging
import pickle
from pathlib import Path
import collections

import numpy
import spacy
import pandas
import umap
import hdbscan

logging.basicConfig()

class ColumnSimilarityLabeler():
    # Save dir is always relative to Path.home()
    def __init__(self,
               save_to_file=True,
               save_dir="data/processed"):

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.logger.info("Logger loaded")

        if isinstance(save_dir, Path):
            self.save_path = save_dir
        elif isinstance(save_dir, str):
            self.save_path = Path(Path.home(), save_dir)
        else:
            raise ValueError("No valid path passed to ColumnSimilarityLabeler for save path")

        self.save_path.mkdir(exist_ok=True)
        self.save_to_file = save_to_file

        self.nlp_engine = spacy.load("en_core_web_lg")

        self.source_dataframe = None
        self.column_reference = None
        self.common_labels = None
        self.column_name = None

    def append_common_labels_for_column(self, source_dataframe, column_name):
        """Adds a common labeling to a column containing text.  This labeling will be the most common words pertaining to the common grouping.

        :param source_dataframe:
        :param column_name:
        :returns:
        :rtype:

        """
        self.source_dataframe = source_dataframe
        self.column_name = column_name
        self._clean_column_field()
        self.column_reference = pandas.DataFrame()
        self.column_reference['column_uniques'] = self.source_dataframe[column_name].unique()
        self.logger.info(f"Unique values from column {self.column_name} loaded")
        self._generate_clusters_from_column()
        self._get_common_name_for_column_labels()
        self._add_labels_to_source_dataframe()
        if self.save_to_file:
            self._save_labelled_dataframe_and_reference()
        return self.source_dataframe

    def build_searchable_field(self):
        # Handle this later
        pass

    def _clean_column_field(self):
        # Anything else goes in here
        self.source_dataframe[self.column_name] = self.source_dataframe[self.column_name].str.replace(
            '&', 'and', regex=False
        )
        self.source_dataframe[self.column_name] = self.source_dataframe[self.column_name].str.lower()

    def _generate_clusters_from_column(self):
        word_vector_list = [doc.vector for doc in self.nlp_engine.pipe(list(self.column_reference.column_uniques))]
        word_vector_matrix = numpy.vstack(word_vector_list)
        self.logger.info(f"Word vector shape : {word_vector_matrix.shape}")
        self.logger.info("Umapping")
        embedding_matrix = umap.UMAP(n_neighbors=10, min_dist=0.0, n_components=40, random_state=42).fit_transform(word_vector_matrix)
        self.logger.info("HDBScanning")
        self.column_reference['label_num'] = hdbscan.HDBSCAN(min_cluster_size=20).fit_predict(embedding_matrix)
        self.logger.info(f"Total number of labels : {len(self.column_reference.label_num.unique())}")

    def _get_common_name_for_column_labels(self):
        groupby_column = self.column_reference.groupby(by='label_num')
        group_labels = [label for label in groupby_column.groups]

        self.logger.info(f"{group_labels}")
        common_name = []
        for group_label in group_labels:
            group_dataframe = groupby_column.get_group(group_label)
            column_document = " ".join(group_dataframe.column_uniques)

            tokens = [token.lemma_ for token in self.nlp_engine(column_document)]
            token_counter = collections.Counter(tokens)
            most_common_tokens = token_counter.most_common(5)
            self.logger.info(f"Label {group_label} : {most_common_tokens}")
            common_name.append("; ".join([text[0] for text in most_common_tokens]))
        self.common_labels = pandas.DataFrame()
        self.common_labels['label_num'] = group_labels
        self.common_labels['common_name'] = common_name
        self.logger.info(f"{self.common_labels.head()}")

    def _add_labels_to_source_dataframe(self):
        self.column_reference.join(self.common_labels, on='label_num')
        self.source_dataframe.join(self.column_reference, left_on=self.column_name, right_on='column_uniques')

    def _save_labelled_dataframe_and_reference(self):
        self.column_reference.to_csv(Path(self.save_path, f"{self.column_name}_reference.csv"))
        self.source_dataframe.to_csv(Path(self.save_path, f"{self.column_name}_completed_data.csv"))
