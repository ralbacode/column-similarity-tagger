import sys
from pathlib import Path
import pickle

import pandas

from column_similarity import build_similarity_for_column


if __name__ == '__main__':
    # Helper script to run column similarity parser for testing purposes
    data_path = Path(Path.home(), "data/")
    sales_data = pandas.read_csv(Path(data_path, "raw", "orders.csv"))
    sales_data["date_purchased"] = pandas.to_datetime(sales_data.date_purchased)
    product_similarity_comparison = build_similarity_for_column(sales_data, "product_name")
    pickle.dump(product_similarity_comparison,
                open(Path(data_path, "train", "similarity_comparison.pkl"), "wb"))
